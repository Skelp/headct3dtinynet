## HeadCT3DTinyNet

This repository consists of the architecture, training and inference for a fully 3D convolutional neural network.
Said architecture was designed for the kaggle competition [SPR Head CT Age Prediction Challenge](https://www.kaggle.com/competitions/spr-head-ct-age-prediction-challenge) and aims to predict the age of a patient based on one or more head CT.

![](https://www.googleapis.com/download/storage/v1/b/kaggle-forum-message-attachments/o/inbox%2F5958987%2F274fde73db96188f794f53dac42a0a6f%2Fdiagram.png?generation=1714628266825394&alt=media)

**Model Architecture and Features:**
- **Parameters**: The model is relatively lightweight with approximately 2.3 million parameters.
- **Attention Mechanisms**: It incorporates both channel and spatial attention mechanisms, enhancing its ability to focus on relevant features within the CT scans.
- **Activation Function**: A key innovation in my model is the novel activation function, APTx. The exact function used is a per-channel-parameterized version of APTx, designed to emulate the performance of the MISH activation function but with reduced computational demand.
- **Dimensionality**: The architecture is a full 3D CNN, leveraging the spatial context better than the commonly used 2.5D or 2D approaches.
- **Imaging Parameters**: For preprocessing, I used a single windowing parameter set at 40, 80 to standardize the input data.
- **Data Augmentation**: Utilized TorchIO for extensive data augmentation, which was critical in enhancing the model's robustness and generalizability.
- **Optimization**: Initially, I used the AdaBelief optimizer with a learning rate of 3e-4. Upon observing plateaus in the leaderboard scores, I adjusted the learning rate to 3e-5 and introduced a weight decay of 0.5 for more stringent regularization, which significantly improved the model's performance.

These choices, particularly the use of a full 3D approach and the innovative APTx activation function, were pivotal in differentiating my model from others and achieving the high accuracy necessary to excel in this competition.

**Detailed Model Architecture:**

My model's architecture is designed to effectively process 3D CT scan data for age prediction. Below are the specifics of the architecture, visualized in the attached diagram:

- **Input Layer**: Accepts 3D data of size 128x128x128.
- **Convolutional Blocks**: The model consists of a series of convolutional blocks (ConvBlock 1 to 6), each followed by 3D max pooling to reduce dimensionality while capturing the most salient features.
  - **Block 1**: Outputs 32 channels, uses Group Normalization (groups=32, effectively instance norm), and includes both Channel Attention 3D and Spatial Attention 3D mechanisms. The APTx Activation, a novel activation function I developed, is used here.
  - **Subsequent Blocks**: Increase in channel output progressively (64, 96, 128, 160, and finally 192).

- **Flattening Layer**: After the convolutional blocks, the data is flattened to feed into the fully connected layers.

- **Fully Connected Layer**: A dense layer that reduces the high-level features into a single prediction output.

- **Output**: The mean of predictions is calculated to determine the final age estimate. Specifically, this refers to the mean of predictions from one or more head CT scans per patient. To handle multiple scans efficiently, I parallelized the processing by placing each scan into the batch dimension. This approach ensures that the model can manage additional data without compromising on processing speed or accuracy.

This architecture leverages attention mechanisms to focus on important spatial and channel-specific features, which is crucial for accurately interpreting medical images like CT scans. The use of APTx activation across the model helps to maintain computational efficiency while achieving performance comparable to more computationally intensive activations like MISH and also learn the optimal parameters per channel.

## References

### Activation Function
- Kumar, R. (2022). APTx: better activation function than MISH, SWISH, and ReLU's variants used in deep learning. *International Journal of Artificial Intelligence and Machine Learning, 2*(2), 56-61. [DOI](https://doi.org/10.51483/IJAIML.2.2.2022.56-61) and [arXiv](https://ar5iv.org/abs/2209.06119).

### Attention Mechanisms
- Woo, S., Park, J., Lee, J. Y., & Kweon, I. S. (2018). CBAM: Convolutional Block Attention Module. *Proceedings of the European Conference on Computer Vision (ECCV)*. [arXiv](https://arxiv.org/abs/1807.06521).

### Optimizer
- Zhuang, J., Tang, T., Ding, Y., Tatikonda, S., Dvornek, N. C., Papademetris, X., & Duncan, J. S. (2020). AdaBelief Optimizer: Adapting Stepsizes by the Belief in Observed Gradients. *Advances in Neural Information Processing Systems*. [arXiv](https://arxiv.org/abs/2010.07468).


