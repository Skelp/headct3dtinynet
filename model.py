import torch
import torch.nn as nn
import pytorch_lightning as pl
from adabelief_pytorch import AdaBelief
from torchmetrics import MaxMetric, MinMetric, MeanMetric
from IPython.display import clear_output
import matplotlib.pyplot as plt

# borrowed from the APTx implementation for pytorch: https://gitlab.com/Skelp/aptx
# changed the code to have learnable parameters per channel
class APTx(nn.Module):
    def __init__(self, num_channels, alpha=1.0, beta=1.0, gamma=0.5):
        super(APTx, self).__init__()
        self.alpha = nn.Parameter(torch.full((num_channels,), alpha, dtype=torch.float32))
        self.beta = nn.Parameter(torch.full((num_channels,), beta, dtype=torch.float32))
        self.gamma = nn.Parameter(torch.full((num_channels,), gamma, dtype=torch.float32))
        
    def forward(self, x):
        # Adjust the shape of the parameters to match the input dimensions
        # This assumes the channel dimension is always the second one (as in NCHW or similar formats)
        dims = [1, -1] + [1] * (x.dim() - 2)
        alpha = self.alpha.view(*dims)
        beta = self.beta.view(*dims)
        gamma = self.gamma.view(*dims)
        tanh_beta_x = torch.tanh(beta * x)
        phi_x = (alpha + tanh_beta_x) * gamma * x
        return phi_x
    
    def extra_repr(self):
        return 'num_channels={}, alpha={}, beta={}, gamma={}'.format(self.alpha.numel(), self.alpha[0].item(),
                                                                     self.beta[0].item(), self.gamma[0].item())
    
# both ChannelAttention and SpatialAttention were inspired by CBAM, implemented in pytorch and adapted for 3D 
# additionally, I added more complexity, but I have not checked whether this increases performance or not
class ChannelAttention3D(nn.Module):
    def __init__(self, in_channels, reduction_ratio=1):
        super(ChannelAttention3D, self).__init__()
        reduced_channels = in_channels // reduction_ratio  # Ensure at least 4 channels
        self.avg_pool = nn.AdaptiveAvgPool3d(1)  # Adjusted for 3D
        self.max_pool = nn.AdaptiveMaxPool3d(1)  # Adjusted for 3D
        self.fc = nn.Sequential(
            nn.Linear(in_channels, reduced_channels),
            nn.LayerNorm(reduced_channels),
            APTx(reduced_channels),
            nn.Linear(reduced_channels, reduced_channels),
            nn.LayerNorm(reduced_channels),
            APTx(reduced_channels),
            nn.Linear(reduced_channels, reduced_channels),
            nn.LayerNorm(reduced_channels),
            APTx(reduced_channels),
            nn.Linear(reduced_channels, in_channels),
        )
        self.sigmoid = nn.Sigmoid()
        
    def forward(self, x):
        b, c, _, _, _ = x.size()  # Adjusted for 3D
        # Squeeze
        y_a = self.avg_pool(x).view(b, c)
        y_m = self.max_pool(x).view(b, c)
        # Excitation
        y_a = self.fc(y_a).view(b, c, 1, 1, 1)  # Adjusted for 3D
        y_m = self.fc(y_m).view(b, c, 1, 1, 1)  # Adjusted for 3D
        # Scale
        return x * self.sigmoid(y_a.expand_as(x) + y_m.expand_as(x))
    
class SpatialAttention3D(nn.Module):
    def __init__(self, kernel_size=5):
        super(SpatialAttention3D, self).__init__()
        padding = kernel_size // 2
        self.norm = nn.GroupNorm(1, 2, affine=True)
        self.conv1 = nn.Conv3d(2, 2, kernel_size, padding=padding)  # Adjusted for 3D
        self.activation = APTx(2)
        self.conv2 = nn.Conv3d(2, 1, kernel_size, padding=padding)  # Adjusted for 3D
        self.sigmoid = nn.Sigmoid()
        
    def forward(self, x):
        avg_out = torch.mean(x, dim=1, keepdim=True)
        max_out, _ = torch.max(x, dim=1, keepdim=True)
        x_m = torch.cat([avg_out, max_out], dim=1)
        x_m = self.norm(x_m)
        x_m = self.conv1(x_m)
        x_m = self.activation(x_m)
        x_m = self.conv2(x_m)
        return x * self.sigmoid(x_m)
    
class ConvBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=3, stride=1, padding=1, module=nn.Conv3d, p=0.2):
        super(ConvBlock, self).__init__()
        self.conv = module(in_channels, out_channels, kernel_size, stride=stride, padding=padding, bias=False)
        self.se = ChannelAttention3D(out_channels)
        self.sa = SpatialAttention3D()
        self.norm = nn.GroupNorm(1, out_channels, affine=True)
        self.activation = APTx(out_channels)
        
    def forward(self, x):
        x = self.conv(x)
        x = self.activation(x)
        x = self.se(x)
        x = self.sa(x)
        return self.norm(x)

class HeadCT3DTinyNet(pl.LightningModule):
    def __init__(self):
        super().__init__()
        self.loss_history = []
        self.lr_history = []
        self.weight_history = []
        self.loss_fn = nn.L1Loss()
        self.min_metric = MinMetric()
        self.max_metric = MaxMetric()
        self.mean_metric = MeanMetric()
        self.reduce = nn.MaxPool3d(2, stride=2)
        self.norm = nn.GroupNorm(1, 1, affine=True)
        self.conv1 = ConvBlock(1, 32)  # 128x128x128
        self.conv2 = ConvBlock(32, 64)  # 64x64x64
        self.conv3 = ConvBlock(64, 96)  # 32x32x32
        self.conv4 = ConvBlock(96, 128)  # 16x16x16
        self.conv5 = ConvBlock(128, 160)  # 8x8x8
        self.conv6 = ConvBlock(160, 192)  # 4x4x4
        self.lin = nn.Linear(4 * 4 * 4 * 192, 1)
        
    def forward(self, x):
        x = x.squeeze(0)
        x = self.norm(x)
        x = self.conv1(x)
        x = self.reduce(x)
        x = self.conv2(x)
        x = self.reduce(x)
        x = self.conv3(x)
        x = self.reduce(x)
        x = self.conv4(x)
        x = self.reduce(x)
        x = self.conv5(x)
        x = self.reduce(x)
        x = self.conv6(x)
        x = torch.flatten(x, 1)
        x = self.lin(x)
        x = torch.mean(x).unsqueeze(0)
        return x
    
    def training_step(self, batch, batch_idx):
        images, ages = batch
        predictions = self(images)
        self.min_metric.update(predictions)
        self.max_metric.update(predictions)
        self.mean_metric.update(predictions)
        log_loss = nn.functional.l1_loss(predictions, ages)
        loss = self.loss_fn(predictions, ages)
        
        if self.current_epoch < 2:
            self.log('pred val', predictions, on_epoch=True, prog_bar=True)
            
        self.log('train_loss', log_loss, on_epoch=True, prog_bar=True)
        return loss
    
    def on_train_epoch_end(self):
        clear_output(wait=True)
        self.log('min_pred', self.min_metric.compute(), on_epoch=True, prog_bar=True)
        self.log('max_pred', self.max_metric.compute(), on_epoch=True, prog_bar=True)
        self.log('mean_pred', self.mean_metric.compute(), on_epoch=True, prog_bar=True)

        # Reset metrics for the next epoch
        self.val_min_metric.reset()
        self.val_max_metric.reset()
        self.val_mean_metric.reset()
        
        self.loss_history.append(self.trainer.callback_metrics['train_loss'].item())
        
        current_lr = self.trainer.optimizers[0].param_groups[0]['lr']
        self.lr_history.append(current_lr)
        
        weight_norm = torch.norm(torch.stack([torch.norm(p, 2) for p in self.parameters()]), 2).item()
        self.weight_history.append(weight_norm)
        
        plt.figure(figsize=(15, 4))
        plt.subplot(1, 3, 1)
        plt.plot(self.loss_history, '-o')
        plt.yscale('log')
        plt.title('Loss History')
        plt.xlabel('Epoch')
        plt.ylabel('Loss')
        plt.subplot(1, 3, 2)
        plt.plot(self.weight_history, '-o')
        plt.title('Weight Norm History')
        plt.xlabel('Epoch')
        plt.ylabel('L2 Norm')
        plt.subplot(1, 3, 3)
        plt.plot(self.lr_history, '-o')
        plt.yscale('log')
        plt.title('Learning Rate History')
        plt.xlabel('Epoch')
        plt.ylabel('Learning Rate')
        plt.tight_layout()
        plt.show()
        print("Loss: " + str(self.loss_history[-1]))
        print("min: " + str(self.trainer.callback_metrics['min_pred'].item()))
        print("max: " + str(self.trainer.callback_metrics['max_pred'].item()))
        print("mean: " + str(self.trainer.callback_metrics['mean_pred'].item()))
        
    def configure_optimizers(self):
        optimizer = AdaBelief(self.parameters(), lr=0.3e-3, eps=1e-8, weight_decay=2e-3)
        return optimizer